import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PiechartService {
  apiUrl: string = 'https://api.mocki.io/v1/b043df5a';
  headers = new HttpHeaders().set('Content-Type', 'application/json');

  constructor(private http: HttpClient) { }
  //get Data for pieseries
  showTasks() {
    return this.http.get("https://api.mocki.io/v1/b043df5a");
  }
  //get Data for pieseries1
  showTasks1(){
    return this.http.get("https://api.mocki.io/v1/b043df5a")
  }
  //get Data for pieseries2
  showTasks2(){
    return this.http.get("https://api.mocki.io/v1/b043df5a")
  }
}

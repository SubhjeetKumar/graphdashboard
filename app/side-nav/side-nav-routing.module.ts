import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SideNavComponent } from './side-nav.component';
import {MainComponent } from './main/main.component'
import { Pieseries1Component } from './pieseries1/pieseries1.component';
import { Pieseries2Component } from './pieseries2/pieseries2.component';
import { PieseriesComponent } from './pieseries/pieseries.component';
const routes: Routes = [
  {
    path:'',
    component:SideNavComponent,
    children:[
      {
        path:'UserRegisterApplication',
        component:Pieseries1Component
      },
      {
        path:'NoCustomSubscription',
        component:Pieseries2Component
      },
      {
        path:'UserConsentToApp',
        component:PieseriesComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SideNavRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SideNavRoutingModule } from './side-nav-routing.module';
import { SideNavComponent } from './side-nav.component';
import { MainComponent } from './main/main.component';
import { Pieseries1Component } from './pieseries1/pieseries1.component';
import { Pieseries2Component } from './pieseries2/pieseries2.component';
import { PieseriesComponent } from './pieseries/pieseries.component';
import { jqxChartModule } from 'jqwidgets-ng/jqxchart';
import { MatGridListModule } from '@angular/material/grid-list';
import {MatTableModule} from '@angular/material/table';




@NgModule({
  declarations: [
    SideNavComponent,
    MainComponent,
    Pieseries1Component,
    Pieseries2Component,
    PieseriesComponent,
  ],
  imports: [
    CommonModule,
    SideNavRoutingModule,
    jqxChartModule,
    MatGridListModule,
    MatTableModule
  ]
})
export class SideNavModule { }

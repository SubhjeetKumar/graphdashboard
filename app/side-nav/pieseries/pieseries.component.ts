import { Component,ViewChild, ElementRef, OnInit } from '@angular/core';
import {PiechartService} from '../../piechart.service';



@Component({
  selector: 'app-pieseries',
  templateUrl: './pieseries.component.html',
  styleUrls: ['./pieseries.component.css']
})


export class PieseriesComponent implements OnInit {
    eventData:any=[];
    data:any=[];
    @ViewChild('eventText', { static: false }) eventText:any | ElementRef;
  constructor(private piechartService: PiechartService) { }

  ngOnInit(): void {
   this.show();
  }

  show():void{
    this.piechartService.showTasks()
    .subscribe(
      data=>{
        this.data=data;
      console.log(data)
      }
    )
  }
  Click(event: any): void
  {console.log('jgj')
      // Do Something
  }
  yuser=[{"name":"Ram","location":"Asia","status":"active"},
{"name":"Lokesh","location":"Asia","status":"active"}
]

 nuser=[{name:"Radhika",location:"Africa",status:"active"},
 {name:"Somesh",location:"Africa",status:"active"},
 {name:"Abhishek",location:"Asia",status:"active"},
 {name:"Ryan",location:"Africa",status:"active"},
 {name:"Roy",location:"Asia",status:"active"},
 {name:"Latika",location:"Africa",status:"active"},
 {name:"Rika",location:"Africa",status:"active"},
 {name:"Rina",location:"Asia",status:"active"}
]
  source: any =
  [
    { Browser: 'Yes', Share: 20 },
    { Browser: 'No', Share: 80 },

  ];

  dataAdapter: any = new jqx.dataAdapter(this.source, { async: false, autoBind: true, loadError: (xhr: any, status: any, error: any) => { alert('Error loading "' + this.source.url + '" : ' + error); } });
  padding: any = { left: 5, top: 5, right: 5, bottom: 5 };
  titlePadding: any = { left: 0, top: 0, right: 0, bottom: 10 };
  getWidth() : any {
      if (document.body.offsetWidth < 850) {
          return '90%';
      }

      return 850;
  }

  seriesGroups: any[] =
  [
      {
          type: 'pie',
          showLabels: true,
          toolTipFormatFunction: (
            value:any,
            itemIndex:any,
            serie:any,
            group:any,
            xAxisValue:any,
            xAxis:any
          ) => {
          console.log('serie.displayText', serie, value, xAxisValue, xAxis, group, itemIndex)
          var dataItem = this.source[itemIndex];
            return (
              "<div style='font-size:30px'>" +
              dataItem.Browser + " : " +
              value/10 +
            "</div>"
            );
          },
          series:
          [
              {
                  dataField: 'Share',
                  displayText: 'Browser',
                  labelRadius: 100,
                  initialAngle: 15,
                  radius: 200,
                  centerOffset: 0,
                  toolTipSize:25,
                  formatSettings: { sufix: '%', decimalPlaces: 0 }
              }
          ]
      }
  ];
  chartEvent(event: any): any {
    if (event) {
        if (event.args) {
            if (event.type == 'toggle') {
                // this.eventData = '<div><b>Last Event: </b>' + event.type + '<b>, Serie DataField: </b>' + event.args.serie.dataField + '<b>, visible: </b>' + event.args.state + '</div>';
                return;
            }
            if( event.args.elementValue==10*this.yuser.length){
               this.eventData = this.yuser
               console.log('lll',this.eventData)
               console.log('event args',event.args)
               console.log('browser',this.source[event.args.elementIndex].Browser)
            }
            if(event.args.elementValue==10*this.nuser.length){
                this.eventData  = this.nuser
                console.log('lllsds',this.eventData)
                console.log('browser',this.source[event.args.elementIndex].Browser)
            }
            //eventData = '<ul>'+'<li>Ram</li>'+'</ul>'
           //eventData = '<div><b>Last Event: </b>' + event.type + '<b>, Serie DataField: </b>' + event.args.serie.dataField + '<b>, Value: </b>' + event.args.elementValue + '</div>';
        } else {
            this.eventData  = '<div><b>Last Event: </b>' + event.type + ' hel ';
        }
        // this.eventText.nativeElement.innerHTML =  this.eventData ;
    }
}
}

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PieseriesComponent } from './pieseries.component';

describe('PieseriesComponent', () => {
  let component: PieseriesComponent;
  let fixture: ComponentFixture<PieseriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PieseriesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PieseriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

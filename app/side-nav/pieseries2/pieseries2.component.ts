import { Component, ViewChild, ElementRef,OnInit } from '@angular/core';
import {PiechartService} from '../../piechart.service';
@Component({
  selector: 'app-pieseries2',
  templateUrl: './pieseries2.component.html',
  styleUrls: ['./pieseries2.component.css']
})
export class Pieseries2Component implements OnInit {
    eventData:any=[]
    @ViewChild('eventText', { static: false }) eventText:any | ElementRef;
  constructor(private piechartService: PiechartService) { }

  ngOnInit(): void {}
  Click(event: any): void
  {
      // Do Something
  }
yuser=[{"name":"Ram","location":"Asia","status":"active"},
{"name":"Lokesh","location":"Asia","status":"active"},
{"name":"Yogesh","location":"Europe","status":"active"},
{"name":"Sau","location":"Europe","status":"active"},
{"name":"Evan","location":"America","status":"active"},
{"name":"Loia","location":"Europe","status":"active"},
]

 nuser=[{"name":"Radhika","location":"Africa","status":"active"},
 {"name":"Kane","location":"Europe","status":"active"},
 {"name":"Roy","location":"Asia","status":"active"},
 {"name":"Mahinder","location":"America","status":"active"},]

source: any =
    [
        { Browser: 'Yes', Share: 60 },
        { Browser: 'No', Share: 40 },

    ];

  dataAdapter: any = new jqx.dataAdapter(this.source, { async: false, autoBind: true, loadError: (xhr: any, status: any, error: any) => { alert('Error loading "' + this.source.url + '" : ' + error); } });
    padding: any = { left: 5, top: 5, right: 5, bottom: 5 };
    titlePadding: any = { left: 0, top: 0, right: 0, bottom: 10 };
	getWidth() : any {
		if (document.body.offsetWidth < 850) {
			return '90%';
		}

		return 850;
	}


    seriesGroups: any[] =
    [
        {
            type: 'pie',
            showLabels: true,
            toolTipFormatFunction: (
                value:any,
                itemIndex:any,
                serie:any,
                group:any,
                xAxisValue:any,
                xAxis:any
              ) => {
              console.log('serie.displayText', serie, value, xAxisValue, xAxis, group, itemIndex)
              var dataItem = this.source[itemIndex];
                return (
                  "<div style='font-size:30px'>" +
                  dataItem.Browser + " : " +
                  value/10 +
                "</div>"
                );
              },
            series:
            [
                {
                    dataField: 'Share',
                    displayText: 'Browser',
                    labelRadius: 100,
                    initialAngle: 15,
                    radius: 200,
                    centerOffset: 0,
                    formatSettings: { sufix: '%', decimalPlaces: 0 }
                }
            ]
        }
    ];




  chartEvent(event: any): any {
            let eventData;
            if (event) {
                if (event.args) {
                    if (event.type == 'toggle') {
                        eventData = '<div><b>Last Event: </b>' + event.type + '<b>, Serie DataField: </b>' + event.args.serie.dataField + '<b>, visible: </b>' + event.args.state + '</div>';
                        return;
                    }
                    if( event.args.elementValue==40){
                        this.eventData = this.nuser
                        console.log('lll',this.eventData)
                     }
                     if(event.args.elementValue==60){
                         this.eventData  = this.yuser
                         console.log('lllsds',this.eventData)
                     }
                    // eventData = '<div><b>Last Event: </b>' + event.type + '<b>, Serie DataField: </b>' + event.args.serie.dataField + '<b>, Value: </b>' + event.args.elementValue + '</div>';
                } else {
                    eventData = '<div><b>Last Event: </b>' + event.type + '';
                }
                // this.eventText.nativeElement.innerHTML = eventData;
            }
        }

}

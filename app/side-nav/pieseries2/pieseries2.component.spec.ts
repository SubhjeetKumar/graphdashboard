import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pieseries2Component } from './pieseries2.component';

describe('Pieseries2Component', () => {
  let component: Pieseries2Component;
  let fixture: ComponentFixture<Pieseries2Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pieseries2Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pieseries2Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Pieseries1Component } from './pieseries1.component';

describe('Pieseries1Component', () => {
  let component: Pieseries1Component;
  let fixture: ComponentFixture<Pieseries1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Pieseries1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Pieseries1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { jqxBarGaugeModule }  from 'jqwidgets-ng/jqxbargauge';
import { jqxChartModule } from 'jqwidgets-ng/jqxchart';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LineseriesComponent } from './chartprac/lineseries/lineseries.component';
import { WaterfallComponent } from './waterfall/waterfall.component';
import { BubbleComponent } from './chartprac/bubble/bubble.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatGridListModule} from '@angular/material/grid-list';



@NgModule({
  declarations: [
    AppComponent,
    LineseriesComponent,

    WaterfallComponent,
    BubbleComponent,


  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    jqxBarGaugeModule,
    jqxChartModule,
    BrowserAnimationsModule,
    MatGridListModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

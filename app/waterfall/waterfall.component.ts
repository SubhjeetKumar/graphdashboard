import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-waterfall',
  templateUrl: './waterfall.component.html',
  styleUrls: ['./waterfall.component.css']
})
export class WaterfallComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  sampleData: any[] = [
    { year: '2006', populationChange: 10, Erica: 10, George: 25 },
    { year: '2010', populationChange: 5, Erica: 5, George: 30 },
    { year: '2014', populationChange: 19, Erica: 9, George: 25 },
    { year: '2018', populationChange: 5, Erica: 12, George: 45 },
    { year: '2022', populationChange: 20, Erica: 4, George: 25 },
    { year: '2024', populationChange: 30, Erica: 9, George: 30 },
    { year: '2028', populationChange: 10, Erica: 1, George: 90 }
];
getWidth() : any {
    if (document.body.offsetWidth < 850) {
        return '90%';
    }
    
    return 850;
}

 padding: any = { left: 5, top: 5, right: 5, bottom: 5 };
// titlePadding: any = { left: 90, top: 0, right: 0, bottom: 10 };
xAxis: any =
{
    dataField: 'year',
    showGridLines: true
};
seriesGroups: any[] =
[
    {
        type: 'waterfall',
        columnsGapPercent: 50,
        seriesGapPercent: 0,
        valueAxis:
        {
            unitInterval: 10,
            minValue: 0,
            maxValue: 100,
            displayValueAxis: true,
            description: 'Time in minutes',
            // axisSize: 'auto',
            tickMarksColor: '#888888'
        },
        series: [
            { dataField: 'populationChange', displayText: 'Keith' },
        ]
    }
];

}

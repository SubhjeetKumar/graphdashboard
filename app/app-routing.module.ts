import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "common",
    loadChildren: () =>
      import("./side-nav/side-nav.module").then((m) => m.SideNavModule),
  },
  {
    path: '',
    pathMatch: 'full',
    redirectTo: '/common'
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
